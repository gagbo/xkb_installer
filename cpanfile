requires "XML::Twig", "3.52";
requires "Tie::IxHash";
requires "List::Util";

on 'develop' => sub {
  requires "Perl::LanguageServer", "2.3.0"
}
