#!/usr/bin/env perl

# The aim of this script is to ease the installation of custom xkb keyboard layouts

use warnings;
use strict;

use XML::Twig;
use List::Util qw(none);

# TODO: Make these script arguments
my $xkb_cat = "opti";
my $xkb_name = "optimot_ergo";
my $xkb_description = "French (Optimot, clavier Ergo)";

# Return the variant to add on the .lst files
sub lstEntry {
    # Assert that scalar(@_) == 3

    my $name = $_[0];
    my $cat = $_[1];
    my $desc = $_[2];
    return "$name\t$cat: $desc"
}

# Add a keyboard layout to the file given in RULE_FILE_PATH
sub addLayout {
    my $rule_file_path = $_[0];
    my $name = $_[1];
    my $cat = $_[2];
    my $desc = $_[3];

    my $rules = XML::Twig->new(
        twig_roots => { 'xkbConfigRegistry/layoutList' => sub {
            my ($twig, $layoutList) = @_;

            # Check whether the custom rule is already installed before doing anything
            my @layouts = $layoutList->children();
            my $misses_layout = none {
                $_->first_child("configItem[string(name)= \"$name\"]")
            } @layouts;

            if ($misses_layout) {
                # Create the element and print it
                my $rule = XML::Twig::Elt->new(
                    'layout',
                    XML::Twig::Elt->new(
                        'configItem',
                        XML::Twig::Elt->new('name', $name),
                        XML::Twig::Elt->new('shortDescription', $name),
                        XML::Twig::Elt->new('description', "$name keyboard layout"),
                        XML::Twig::Elt->new('languageList',
                            # TODO: this list shouldn't be hardcoded but should be created from a mapping
                            # operation from a list of language IDs to a list of Twig elements.
                            # Probably as a sub (@languageIds) -> @XML::Twig::Elt
                            XML::Twig::Elt->new('iso639Id', 'eng'),
                            XML::Twig::Elt->new('iso639Id', 'fr'),
                            ),
                        ),
                    XML::Twig::Elt->new('variantList',
                        XML::Twig::Elt->new('variant',
                            XML::Twig::Elt->new('configItem',
                                XML::Twig::Elt->new('name', $name),
                                XML::Twig::Elt->new('description', $desc),),
                        ),
                    )
                    );
                $rule->paste('last_child', $_);
            }

            $layoutList->print;
            $layoutList->purge;
            return;
            },
        },
        pretty_print => 'indented_c',
        twig_print_outside_roots => 1,
        keep_atts_order => 1,
        );
    $rules->parsefile_inplace($rule_file_path, '.bak');
    $rules->flush;

}

# In /usr/share/X11/xkb/rules/base.lst, after the `^!\s*variant\s*$` line
# In /usr/share/X11/xkb/rules/evdev.lst, after the `^!\s*variant\s*$` line
print lstEntry($xkb_name, $xkb_cat, $xkb_description);

# addLayout("/usr/share/X11/xkb/rules/base.xml", $xkb_name, $xkb_cat, $xkb_description);
# addLayout("/usr/share/X11/xkb/rules/evdev.xml", $xkb_name, $xkb_cat, $xkb_description);
addLayout("./test.xml", $xkb_name, $xkb_cat, $xkb_description);
