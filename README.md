# XKB extra layout installer

As it is hard to properly package an XKB layout because there's not only
"extra files" to install but also XML manipulations in root-owned files,
here is a small perl script that automates these steps

# Installation

Fetch all deps with a tool like `cpanminus`

```bash
cpanm --installdeps .
```

# Development

```bash
cpanm --with-develop --installdeps .
```

# License information
The LICENSE only applies to files outside of the `vendor` directory, over which I
have no rights
